﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;

namespace SecretsGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: SecretsGenerator.exe <thumbrpint> <DataToEncrypt>");
                return;
            }

            var thumbprint = args[0];
            var data = args[1];

            if (!Regex.IsMatch(thumbprint, "^[0-9A-Fa-f]+$") && thumbprint.Length != 40)
            {
                Console.WriteLine($"Incorrect thumbprint {thumbprint}. Its length is {thumbprint.Length} symbols.");
                return;
            }

            string result;
            try
            {
                result = Encrypt(data, thumbprint);
            }
            catch (NullReferenceException e)
            {
                throw new Exception("Error. Please check if certificate has private key. ", e);
            }

            Console.WriteLine(result);
        }

        public static string Encrypt(string input, string thumbprint)
        {
            try
            {
                return Encrypt(input, StoreLocation.LocalMachine, thumbprint);
            }
            catch (CertificateNotFoundException)
            {
                return Encrypt(input, StoreLocation.CurrentUser, thumbprint);
            }
        }

        public static string Encrypt(string input, StoreLocation storeLocation, string thumbprint)
        {
            var x509Store = new X509Store(StoreName.My, storeLocation);
            x509Store.Open(OpenFlags.ReadOnly);
            try
            {
                var certificateCollection = x509Store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);
                if (certificateCollection.Count != 1)
                    throw new CertificateNotFoundException();

                using (RSA rsaPrivateKey = certificateCollection[0].GetRSAPrivateKey())
                {
                    var bytes = Encoding.UTF8.GetBytes(input);
                    return Convert.ToBase64String(rsaPrivateKey.Encrypt(bytes, RSAEncryptionPadding.OaepSHA1));
                }
            }
            finally
            {
                x509Store.Close();
            }
        }
    }
}